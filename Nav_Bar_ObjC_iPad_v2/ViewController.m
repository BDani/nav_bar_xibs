//
//  ViewController.m
//  Nav_Bar_ObjC_iPad_v2
//
//  Created by Bruno Tavares on 16/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (id)init
{
    self = [super initWithNibName:@"ViewController" bundle:nil];
    
    self.title = @"View 1";

    return self;
}

- (IBAction)touchButton:(id)sender {
    
    SecondViewController *vc = [[SecondViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    vc.title = @"View 2";
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
