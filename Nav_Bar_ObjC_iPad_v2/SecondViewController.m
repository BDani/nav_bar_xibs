//
//  SecondViewController.m
//  Nav_Bar_ObjC_iPad_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (id)init
{
    self = [super initWithNibName:@"SecondViewController" bundle:nil];
    
    return self;
}

@end
